<?php

use EquationLabs\DBLocker\Utils\URIParser;
use PHPUnit\Framework\TestCase;

class URIParserTest extends TestCase
{

    /** @var URIParser */
    private $parser;

    protected function setUp()
    {
        $this->parser = new URIParser('mysql://user:pass@localhost:3306/database');
    }

    /** @test */
    public function it_can_parse_to_dsn_format()
    {
        $this->assertEquals('mysql:dbname=database;host=localhost;port=3306',
            $this->parser->dsn());
    }

    /** @test */
    public function it_can_parse_to_dsn_with_default_port()
    {
        $parser = new URIParser('mysql://user:pass@localhost/database');

        $this->assertEquals('mysql:dbname=database;host=localhost;port=3306',
            $parser->dsn());
    }

    /** @test */
    public function it_can_parse_to_username()
    {
        $this->assertEquals('user',
            $this->parser->user());
    }

    /** @test */
    public function it_can_parse_to_password()
    {
        $this->assertEquals('pass',
            $this->parser->password());
    }
}