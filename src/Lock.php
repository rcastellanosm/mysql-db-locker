<?php

namespace EquationLabs\DBLocker;

use GeoPagos\DBLocker\Utils\DBHandler;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class Lock extends Command
{

    protected function configure()
    {
        $this->setName('lock')
            ->addOption('uri',
                'u',
                InputOption::VALUE_REQUIRED,
                'Database connection URI')
            ->addOption('locker',
                'l',
                InputOption::VALUE_REQUIRED,
                'Who\'s making the lock',
                'The dark lord');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("\nLocking database\n");

        $locker = $input->getOption('locker');

        $dbHandler = new DBHandler($input->getOption('uri'), $output);

        $dbHandler->createLocksIfNotExists();

        if ($dbHandler->isLocked()) {
            $output->writeln("\n\nDatabase is already locked\n\n");

            return 0;
        }

        $dbHandler->lock($locker);
        $output->writeln("\n\nDatabase locked by $locker!\n\n");

        return 0;
    }
}