<?php

namespace EquationLabs\DBLocker\Utils;


use Exception;
use PDO;
use PDOException;
use Symfony\Component\Console\Output\OutputInterface;

class DBHandler
{

    /** @var PDO */
    private $PDO;

    /** @var URIParser */
    private $parser;

    /** @var OutputInterface */
    private $output;

    public function __construct(string $uri, OutputInterface $output)
    {
        $this->output = $output;
        $this->parser = new URIParser($uri);

        while (!$this->connect($this->parser->dsn(),
            $this->parser->user(),
            $this->parser->password())) {
            sleep(1);
        }
    }

    private function connect(string $dsn, string $user, string $password): bool
    {
        try {
            $this->PDO = new PDO($dsn, $user, $password);
        } catch (PDOException $e) {
            $this->output->writeln("Could not reach database: " . $e->getMessage());

            return false;
        }

        return true;
    }

    public function createLocksIfNotExists()
    {
        $this->PDO->query('CREATE TABLE IF NOT EXISTS locks
                                (
                                    id int PRIMARY KEY AUTO_INCREMENT,
                                    locker varchar(50) NOT NULL,
                                    status tinyint DEFAULT 1 NOT NULL
                                )');
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function isLocked()
    {
        $result = $this->PDO->query('SELECT COUNT(*) FROM locks WHERE status = 1');

        if (!$result) {
            $this->output->writeln('Locks table was not found');

            return true;
        }

        return $result->fetchColumn();
    }

    public function lock($locker)
    {
        $this->PDO->prepare('INSERT INTO locks (locker) VALUES (:locker)')
            ->execute([':locker' => $locker]);
    }

    public function unlock($locker)
    {
        $this->PDO->prepare('UPDATE locks SET status = :status WHERE locker = :locker')
            ->execute([
                ':status' => 0,
                ':locker' => $locker
            ]);
    }

    public function updateAdministrator($email)
    {
        $this->PDO->prepare('UPDATE back_office_user SET email = :email WHERE id = 1')
            ->execute(
                [
                    ':email' => $email
                ]
            );
    }
}

