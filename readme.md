## Overview
The goal of this package is to provide a convention for locking, waiting and unlocking a database.

## Usage

``bin/locker lock --locker backend --uri mysql://user:password@host:port/database``

``bin/locker unlock --locker backend --uri mysql://user:password@host:port/database``

``bin/locker wait --time 5 --uri mysql://user:password@host:port/database``

### Extra

If external unlocking is needed, execute ``UPDATE locks SET status = 0`` on the database.