<?php

namespace EquationLabs\DBLocker;

use GeoPagos\DBLocker\Utils\DBHandler;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class Unlock extends Command
{

    protected function configure()
    {
        $this->setName('unlock')
            ->addOption('uri',
                'u',
                InputOption::VALUE_REQUIRED,
                'Database connection URI')
            ->addOption('locker',
                'l',
                InputOption::VALUE_REQUIRED,
                'Who\'s making the lock',
                'The dark lord');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("\nUnlocking database\n");

        $dbHandler = new DBHandler($input->getOption('uri'), $output);

        if (!$dbHandler->isLocked()) {
            $output->writeln("\n\nDatabase is already unlocked\n\n");

            return 0;
        }

        $locker = $input->getOption('locker');
        $dbHandler->unlock($locker);
        $output->writeln("\n\nDatabase unlocked by $locker!\n\n");

        return 0;
    }
}