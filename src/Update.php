<?php

namespace EquationLabs\DBLocker;


use GeoPagos\DBLocker\Utils\DBHandler;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class Update extends Command
{
    protected function configure()
    {
        $this->setName('update')
            ->addOption('email',
                'e',
                InputOption::VALUE_REQUIRED,
                'Email to update on ultra admin backoffice user')
            ->addOption('uri',
                'u',
                InputOption::VALUE_REQUIRED,
                'Database connection URI');
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dbHandler = new DBHandler($input->getOption('uri'), $output);

        $this->getName();

        $output->writeln("\nUpdating ultra admin for QA purpouses...\n");

        $dbHandler->updateAdministrator($input->getOption('email'));

        $output->writeln("\nNow qa@geopagos.com is the ultra admin.\n");

        return 0;
    }
}