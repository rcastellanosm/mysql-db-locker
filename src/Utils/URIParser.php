<?php

namespace EquationLabs\DBLocker\Utils;

class URIParser
{

    const DEFAULT_MYSQL_PORT = 3306;

    private $uri;

    public function __construct($uri)
    {
        // TODO validate
        $this->uri = $uri;
    }

    public function user()
    {
        return $this->left(':', $this->credentials());
    }

    public function password()
    {
        return $this->right(':', $this->credentials());
    }

    public function dsn()
    {
        return $this->driver() . ':' .
            'dbname=' . $this->database() . ';' .
            'host=' . $this->host() . ';' .
            'port=' . $this->port();
    }

    private function driver()
    {
        return $this->left('://', $this->uri);
    }

    private function socket(): array
    {
        return explode(':',
            $this->left('/', $this->right('@', $this->uri)));
    }

    private function database()
    {
        return $this->right('/',
            $this->right('@', $this->uri));
    }

    private function left($delimiter, $string)
    {
        return explode($delimiter, $string)[0];
    }

    private function right($delimiter, $string)
    {
        return explode($delimiter, $string)[1];
    }

    private function credentials()
    {
        return $this->right('//',
            $this->left('@', $this->uri));
    }

    private function port()
    {
        return $this->socket()[1] ?? self::DEFAULT_MYSQL_PORT;
    }

    private function host()
    {
        return $this->socket()[0];
    }
}