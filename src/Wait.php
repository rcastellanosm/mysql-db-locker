<?php

namespace EquationLabs\DBLocker;

use GeoPagos\DBLocker\Utils\DBHandler;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class Wait extends Command
{

    protected function configure()
    {
        $this->setName('wait')
            ->addOption('uri',
                'u',
                InputOption::VALUE_REQUIRED,
                'Database connection URI')
            ->addOption('time',
                't',
                InputOption::VALUE_REQUIRED,
                'Interval in seconds to check');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dbHandler = new DBHandler($input->getOption('uri'), $output);

        $output->writeln("\nWaiting for database to unlock...\n");

        while ($dbHandler->isLocked()) {
            $output->writeln("\nDatabase locked, waiting...\n");
            sleep($input->getOption('time'));
        }

        $output->writeln("\nDatabase unlocked, carry on!\n");

        return 0;
    }


}